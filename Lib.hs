module Lib (
    addInt,
    piSquare,
    solveQuadraticEquasion,
    fibonacci,
    sumList,
    multList,
    addNandM,
    addLastN,
    fibonacciList,
    fibonacciList2,
    caesarEncrypt,
    caesarDecrypt,
    amountGoodStudents,
    contains,
    contains2,
    contains3,
    contains4,
    reduceR,
    flipList,
    otp,
    vigenere,
    vigenere2,
    listLength,
    absValue,
    quickSort,
    quickSort2,
    BinaryTree(..),
    insertIntoTree,
    binaryTreeSum,
    binaryTreeDepth,
    minInTree,
    leftInTree,
    preOrder,
    inOrder,
    postOrder,
    takeN,
    getHuffmanTree,
    huffmanEncode,
    huffmanDecode
) where

import Data.Char
import Data.Maybe
import Data.Map hiding (map, filter)
import Data.Ratio

-- Aufgabe 2.1
addInt :: Int -> Int -> Int
addInt a b = a + b

-- Aufgabe 2.2
piSquare :: Float
piSquare = pi ^ 2

-- Aufgabe 3
solveQuadraticEquasion :: (Float, Float) -> [Float]
solveQuadraticEquasion (p, q)
    | inSqrt < 0 = [] -- no solution
    | inSqrt == 0 = [-(p / 2)] -- one solution
    | otherwise = [-(p / 2) + sqrt inSqrt, -(p / 2) - sqrt inSqrt] -- two solutions
    where
        inSqrt = (p / 2)**2 - q

-- Aufgabe 4
fibonacci :: Int -> Int
fibonacci 1 = 1
fibonacci 2 = 1
fibonacci n = fibonacci (n - 1) + fibonacci (n - 2)

-- Aufgabe 5.1
sumList :: [Int] -> Int
sumList [] = 0
sumList (x:xs) = x + sumList xs

-- Aufgabe 5.2
multList :: [Int] -> Int
multList [] = 1
multList (x:xs) = x * multList xs

-- Aufgabe 5.3
elementN :: [Int] -> Int -> Int
elementN list 0 = head list
elementN (x:xs) n = elementN xs (n - 1)

addNandM :: [Int] -> Int -> Int -> Int
addNandM list n m = elementN list n + elementN list m

-- Aufgabe 5.4
addLastN :: [Int] -> Int -> Int
-- addLastN list 0 = 0
-- addLastN list n = last list + addLastN (init list) (n - 1)
addLastN list n = if length list == n
    then sum list
    else addLastN (tail list) n

-- Aufgabe 5.5
fibonacciList :: [Int] -> Int -> [Int]
fibonacciList list 0 = list
fibonacciList list n = fibonacciList (list++[addLastN list 2]) (n - 1)

-- Aufgabe 5.6
fibonacciList2 :: Int -> [Int]
fibonacciList2 0 = []
fibonacciList2 1 = [1]
fibonacciList2 2 = [1, 1]
fibonacciList2 n = fibonacciList [1, 1] (n - 2)

-- Präsentation Aufgabe 1
shiftChar :: Char -> Int -> Char
shiftChar c secret = chr ((((ord c) - (ord 'A') + secret) `mod` 26) + (ord 'A'))

caesarEncrypt :: [Char] -> Int -> [Char]
caesarEncrypt word secret = map (\c -> shiftChar c secret) word

-- Präsentation Aufgabe 2
caesarDecrypt :: [Char] -> Int -> [Char]
caesarDecrypt word secret = caesarEncrypt word (-secret)

-- Präsentation Aufgabe 3
amountGoodStudents :: [Int] -> Int
amountGoodStudents mssList = length (filter (\grade -> grade > 10) mssList)

-- contains-Funktionen (in Haskell: elem)
contains :: Eq a => [a] -> a -> Bool
contains [] e = False
contains (x:xs) e = (e == x) || (contains xs e)

contains2 :: Eq a => [a] -> a -> Bool
contains2 list e = or (map (\x -> x == e) list)

contains3 :: Eq a => [a] -> a -> Bool
contains3 [] e = False
contains3 (x:xs) e = if x == e then True else contains3 xs e

contains4 :: Eq a => [a] -> a -> Bool
contains4 list e = length (filter (\x -> x == e) list) > 0

-- reduceR-Funktion (in Haskell: foldr)
reduceR :: (a -> a -> a) -> [a] -> Maybe a
reduceR func [] = Nothing
reduceR func [e] = Just e
reduceR func (x:xs) = case reduceR func xs of
    Just e -> Just (func x e)
    Nothing -> Nothing

-- flipList-Funktion (in Haskell: reverse)
flipList :: [a] -> [a]
flipList [] = []
flipList list = [last list] ++ flipList (init list)

-- One-Time-Pad Chiffre (https://de.m.wikipedia.org/wiki/One-Time-Pad)
otp :: [Char] -> [Char] -> Maybe [Char]
otp [] key = Nothing
otp plaintext key = if (length key) < (length plaintext)
    then Nothing
    else Just (zipWith (\char keyChar -> shiftChar keyChar ((ord char) - (ord 'A'))) plaintext key)

-- Vigenère-Chiffre (https://de.m.wikipedia.org/wiki/Polyalphabetische_Substitution#Vigen.C3.A8re-Verschl.C3.BCsselung)
vigenere :: [Char] -> [Char] -> Maybe [Char]
vigenere [] key = Nothing
vigenere plaintext key = if (length key) < (length plaintext)
    then vigenere plaintext (key ++ key)
    else otp plaintext key

vigenere2 :: [Char] -> [Char] -> [Char]
vigenere2 [] key = []
vigenere2 plaintext key = if (length key) < (length plaintext)
    then vigenere2 plaintext (key ++ key)
    else (shiftChar (head key) ((ord (head plaintext)) - (ord 'A'))):(vigenere2 (tail plaintext) (tail key))

-- listLength-Funktion (in Haskell: length)
listLength :: Eq a => [a] -> Int
listLength list
    | list == [] = 0
    | otherwise = 1 + (listLength (tail list))

-- absValue-Funktion (in Haskell: abs)
absValue :: (Num a, Ord a) => a -> a
absValue n
    | n < 0 = -n
    | otherwise = n

-- QuickSort-Algorithmus (https://de.wikipedia.org/wiki/Quicksort)
quickSort :: Ord a => [a] -> [a]
quickSort list
    | (length list) < 2 = list
    | otherwise = (quickSort smallerList) ++ [pivot] ++ (quickSort largerList)
        where
            pivot = head list
            smallerList = [x | x <- (tail list), x <= pivot]
            largerList = [x | x <- (tail list), x > pivot]

quickSort2 :: Ord a => [a] -> (a -> a -> Bool) -> [a]
quickSort2 list compareF
    | (length list) < 2 = list
    | otherwise = (quickSort2 leftList compareF) ++ [pivot] ++ (quickSort2 rightList compareF)
        where
            pivot = head list
            leftList = filter (\x -> x `compareF` pivot) (tail list)
            rightList = filter (\x -> not (x `compareF` pivot)) (tail list)

-- Binärbaum
data BinaryTree a = Empty
    | Node a (BinaryTree a) (BinaryTree a)
        deriving (Eq, Ord, Read)

instance (Show a) => Show (BinaryTree a) where
    show Empty = "Empty"
    show (Node x leftChild rightChild) = show x ++ " (" ++ show leftChild ++ ") (" ++ show rightChild ++ ")"

insertIntoTree :: Ord a => BinaryTree a -> a -> BinaryTree a
insertIntoTree Empty y = (Node y Empty Empty)
insertIntoTree (Node x leftChild rightChild) y
    | y < x = (Node x (insertIntoTree leftChild y) rightChild)
    | otherwise = (Node x leftChild (insertIntoTree rightChild y))

binaryTreeSum :: Num a => BinaryTree a -> a
binaryTreeSum Empty = 0
binaryTreeSum (Node x leftChild rightChild) = x + (binaryTreeSum leftChild) + (binaryTreeSum rightChild)

binaryTreeDepth :: BinaryTree a -> Int
binaryTreeDepth Empty = 0
binaryTreeDepth (Node x leftChild rightChild) = 1 + max (binaryTreeDepth leftChild) (binaryTreeDepth rightChild)

minInTree :: (Bounded a, Ord a) => BinaryTree a -> a
minInTree Empty = maxBound
minInTree (Node x leftChild rightChild) = min x (min (minInTree leftChild) (minInTree rightChild))

leftInTree :: BinaryTree a -> a
leftInTree (Node x Empty rightChild) = x
leftInTree (Node x leftChild rightChild) = leftInTree leftChild

preOrder :: BinaryTree a -> [a]
preOrder Empty = []
preOrder (Node x leftChild rightChild) = [x] ++ (preOrder leftChild) ++ (preOrder rightChild)

inOrder :: BinaryTree a -> [a]
inOrder Empty = []
inOrder (Node x leftChild rightChild) = (inOrder leftChild) ++ [x] ++ (inOrder rightChild)

postOrder :: BinaryTree a -> [a]
postOrder Empty = []
postOrder (Node x leftChild rightChild) = (postOrder leftChild) ++ (postOrder rightChild) ++ [x]

-- takeN-Funktion (in Haskell: take)
takeN :: Int -> [a] -> Maybe [a]
takeN 0 list = Just []
takeN n [] = Nothing
takeN n (x:xs) = case takeN (n - 1) xs of
    Just es -> Just (x:es)
    Nothing -> Nothing

-- Huffman-Code (https://en.wikipedia.org/wiki/Huffman_coding)

-- Beispiel-Konstruktor: HNode (1 % 1) '0' (HLeaf 'a' (1 % 2) '0') (HNode (1 % 2) '1' (HLeaf 'b' (1 % 5) '0') (HLeaf 'c' (3 % 10) '1'))
data HuffmanTree
    = HLeaf Char (Ratio Int) Char
    | HNode (Ratio Int) Char HuffmanTree HuffmanTree
        deriving (Eq, Ord, Show, Read)

-- Beispiel-Rückgabe für "HALLO": [('A',1), ('H',1), ('L',2), ('O',1)]
countChars :: [Char] -> [(Char, Int)]
countChars word = count word empty
    where
        count :: [Char] -> Map Char Int -> [(Char, Int)]
        count [] counts = toList counts
        count (c:cs) counts =
            let updatedCounts = if member c counts then adjust (+1) c counts else insert c 1 counts
            in count cs updatedCounts

getProbability :: HuffmanTree -> (Ratio Int)
getProbability (HLeaf c p b) = p
getProbability (HNode p b left right) = p

-- p=probability, b=bit, c=char
insertIntoHuffmanQueue :: [HuffmanTree] -> HuffmanTree -> [HuffmanTree]
insertIntoHuffmanQueue [] insertTree = [insertTree]
insertIntoHuffmanQueue queue insertTree = if lastProbability <= insertProbability
    then queue ++ [insertTree]
    else (insertIntoHuffmanQueue (init queue) insertTree) ++ [lastTree]
        where
            lastTree = last queue
            lastProbability = getProbability lastTree
            insertProbability = getProbability insertTree

-- This creates a priority queue in which the elements are sorted from start to end
createHLeafQueue :: [(Char, Int)] -> [HuffmanTree]
createHLeafQueue charCounts =
    let charAmount = sum (map (\(c, a) -> a) charCounts)
    in createQueue charCounts charAmount []
        where
            createQueue :: [(Char, Int)] -> Int -> [HuffmanTree] -> [HuffmanTree]
            createQueue [] charAmount queue = queue
            createQueue charCounts charAmount queue =
                let (c, a) = head charCounts
                    hLeaf = HLeaf c (a % charAmount) '0' -- The bit is not final and will be set correctly in the buildHuffmanTree function
                    newQueue = insertIntoHuffmanQueue queue hLeaf
                in createQueue (tail charCounts) charAmount newQueue

buildHuffmanTree :: [HuffmanTree] -> HuffmanTree
buildHuffmanTree [rootTree] = rootTree
buildHuffmanTree queue =
    let firstChild = setBit (queue !! 0) '0' -- Finally sets the bits 0 and 1 to the children of the new HNode
        secondChild = setBit (queue !! 1) '1'
        restQueue = tail (tail queue)
        newProbability = getProbability firstChild + getProbability secondChild
        newHNode = HNode newProbability '0' firstChild secondChild -- The bit is not final and may be set correctly in one of the next recursive calls
        newQueue = insertIntoHuffmanQueue restQueue newHNode
    in buildHuffmanTree newQueue
        where
            setBit :: HuffmanTree -> Char -> HuffmanTree
            setBit (HLeaf c p b) newB = (HLeaf c p newB)
            setBit (HNode p b left right) newB = (HNode p newB left right)

getBit :: HuffmanTree -> Char
getBit (HLeaf c p b) = b
getBit (HNode p b left right) = b

-- Returns all root-to-leaf paths and adds them to a Map in the format [char1: bits1, ...]
buildHuffmanTable :: HuffmanTree -> Map Char String
buildHuffmanTable huffmanTree = buildTable huffmanTree "" empty
    where
        buildTable :: HuffmanTree -> String -> Map Char String -> Map Char String
        buildTable (HLeaf c p b) bits table = insert c bits table
        buildTable (HNode p b left right) bits table =
            let leftBits = bits ++ [getBit left]
                rightBits = bits ++ [getBit right]
                leftTable = buildTable left leftBits table
                rightTable = buildTable right rightBits table
            in union leftTable rightTable

getHuffmanTree :: String -> HuffmanTree
getHuffmanTree word =
    let charAmount = length word
        charCounts = countChars word
        leafQueue = createHLeafQueue charCounts
    in buildHuffmanTree leafQueue

huffmanEncode :: String -> String
huffmanEncode word =
    let huffmanTree = getHuffmanTree word
    in encodeWord word (buildHuffmanTable huffmanTree)
        where
            encodeWord :: String -> Map Char String -> String
            encodeWord [] huffmanTable = ""
            encodeWord (c:cs) huffmanTable = (findWithDefault "" c huffmanTable) ++ (encodeWord cs huffmanTable) -- findWithDefault looks for the value of c in the table and returns an empty string if the map doesn't contain the char (won't happen)

huffmanDecode :: String -> HuffmanTree -> String
huffmanDecode cipher huffmanTree = decodeBits cipher huffmanTree huffmanTree
    where
        decodeBits :: String -> HuffmanTree -> HuffmanTree -> String
        decodeBits [] (HLeaf c p b) rootTree = [c]
        decodeBits cipher (HLeaf c p b) rootTree = [c] ++ (decodeBits cipher rootTree rootTree)
        decodeBits (bit:bits) (HNode p b left right) rootTree = if bit == (getBit left)
            then decodeBits bits left rootTree
            else decodeBits bits right rootTree
