module Main where

import Lib
import Data.Maybe

main :: IO ()
main = do
    -- Int und Float zu String konvertieren, um sie dann auszugeben:
    putStrLn ("Aufgabe 2.1: " ++ show (addInt 35 7))
    putStrLn ("Aufgabe 2.2: " ++ show piSquare)
    putStr ("Aufgabe 3: \n• " ++ show (solveQuadraticEquasion (2, 1)) ++ "\n• " ++ show (solveQuadraticEquasion (2, -1)) ++ "\n")
    putStrLn ("Aufgabe 4: " ++ show (fibonacci 10))
    putStrLn ("Aufgabe 5.1: " ++ show (sumList [-1, 7, 6, 42]))
    putStrLn ("Aufgabe 5.2: " ++ show (multList [-1, 7, 6, 42]))
    putStrLn ("Aufgabe 5.3: " ++ show (addNandM [-1, 7, 6, 42] 0 3))
    putStrLn ("Aufgabe 5.4: " ++ show (addLastN [42, 2, 3, 4, 10, 10, 4, 3, 2, 13] 4))
    putStrLn ("Aufgabe 5.5: " ++ show (fibonacciList [1, 1] 40))
    putStrLn ("Aufgabe 5.6: " ++ show (fibonacciList2 40))
    putStr ("Aufgabe 6: \n• " ++ show (map (* (-1)) [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377]) ++ "\n• " ++ show (map (+ 100) [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377]) ++ "\n• " ++ show (map (flip mod 7) [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377]) ++ "\n")
    putStrLn ("Präsentation Aufgabe 1: " ++ show (caesarEncrypt "ATTACKATONCE" 4))
    putStrLn ("Präsentation Aufgabe 2: " ++ show (caesarDecrypt "EXXEGOEXSRGI" 4))
    putStrLn ("Präsentation Aufgabe 3: " ++ show (amountGoodStudents [15, 3, 8, 11, 12, 9]))
    putStrLn ("contains-Funktion: " ++ show (contains4 [15, 3, 8, 11, 12, 9] 3))
    putStrLn ("reduce-Funktion (gültig): " ++ show (fromJust (reduceR (+) [1, 2, 3, 4, 5])))
    putStrLn ("reduce-Funktion (ungültig): " ++ show (reduceR (+) []))
    putStrLn ("flipList-Funktion: " ++ show (flipList [3, 1, 4, 1, 5, 9]))
    putStrLn ("One-Time-Pad-Verschlüsselung: " ++ show (fromJust (otp "GEHEIMNIS" "AKEYAKEYA")))
    putStrLn ("Vigenère-Verschlüsselung: " ++ show (fromJust (vigenere "GEHEIMNIS" "AKEY")))
    putStrLn ("Vigenère-Verschlüsselung 2: " ++ show (vigenere2 "GEHEIMNIS" "AKEY"))
    putStrLn ("listLength-Funktion: " ++ show (listLength [1..20]))
    putStrLn ("absValue-Funktion: " ++ show (absValue (-9)))
    putStrLn ("QuickSort: " ++ show (quickSort [5, 1, 2, 15, 99, 53, -5, -67, 4]))
    putStrLn ("QuickSort2: " ++ show (quickSort2 [5, 1, 2, 15, 99, 53, -5, -67, 4] (>)))
    putStrLn ("Binärbaum: " ++ show (Node 'a' (Node 'b' (Node 'c' Empty Empty) (Node 'd' Empty Empty)) (Node 'e' Empty Empty)))
    putStrLn ("Binärbaum inserted: " ++ show ((((((Empty `insertIntoTree` 3) `insertIntoTree` 1) `insertIntoTree` 4) `insertIntoTree` 1)  `insertIntoTree` 5)  `insertIntoTree` 9))
    putStrLn ("Binärbaum-Summe: " ++ show (binaryTreeSum (Node 5 (Node 4 (Node 3 Empty Empty) (Node 1 Empty Empty)) (Node 2 Empty Empty))))
    putStrLn ("Binärbaum-Tiefe: " ++ show (binaryTreeDepth (Node 'a' (Node 'b' (Node 'c' Empty Empty) (Node 'd' Empty Empty)) (Node 'e' Empty Empty))))
    putStrLn ("Binärbaum-MinElement: " ++ show (minInTree (Node 'a' (Node 'b' (Node 'c' Empty Empty) (Node 'd' Empty Empty)) (Node 'e' Empty Empty))))
    putStrLn ("Binärbaum-LeftElement: " ++ show (leftInTree (Node 8 (Node 5 (Node 2 (Node 1 Empty Empty) (Node 3 Empty Empty)) (Node 6 Empty Empty)) (Node 22 (Node 12 Empty Empty) (Node 30 Empty Empty)))))
    putStrLn ("Binärbaum-PreOrder: " ++ show (preOrder (Node 8 (Node 5 (Node 2 (Node 1 Empty Empty) (Node 3 Empty Empty)) (Node 6 Empty Empty)) (Node 22 (Node 12 Empty Empty) (Node 30 Empty Empty)))))
    putStrLn ("Binärbaum-InOrder: " ++ show (inOrder (Node 5 (Node 4 (Node 3 Empty Empty) (Node 1 Empty Empty)) (Node 2 Empty Empty))))
    putStrLn ("Binärbaum-PostOrder: " ++ show (postOrder (Node 5 (Node 4 (Node 3 Empty Empty) (Node 1 Empty Empty)) (Node 2 Empty Empty))))
    putStrLn ("takeN-Funktion: " ++ show (fromJust (takeN 5 (cycle [1..3]))))
    secretText <- readFile "geheimtext.txt"
    putStrLn ("Länge des Geheimtexts: " ++ show (length secretText))
    let huffmanCipher = huffmanEncode secretText
    putStrLn ("Länge des codierten Huffman-Texts: " ++ show (length huffmanCipher))
    writeFile "geheimcode.txt" huffmanCipher
